from flask import Flask, session

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://eskelan:excellant@localhost/eskelan'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True


def is_logged_in():
    if not session:
        return False
    if not 'logged_in' in session:
        return False
    return session['logged_in']
