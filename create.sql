create database if not exists eskelan;

use eskelan;

grant select,update,delete,insert on eskelan.* to 'eskelan'@'localhost' identified by 'excellant';

create table if not exists event_participants (
       id int not null primary key auto_increment,
       participantname varchar(50) not null unique,
       itype varchar(30) not null
);

create table if not exists users (
       id int not null primary key,       
       email varchar(255) not null unique,
       password varchar(512) not null,
       userlevel tinyint not null default 1
);

create table if not exists teams (
       id int not null primary key
);

create table if not exists team_participants (
       user_id int not null,
       team_id int not null,
       ismod tinyint not null default 0,
       primary key(user_id,team_id)
);

create table if not exists images (
       id int not null primary key auto_increment,
       filepath varchar(255) not null,
       title varchar(100)
);

create table if not exists events (
       id int not null primary key auto_increment,
       title varchar(100) not null,
       description varchar(32767) not null,
       starttime TIMESTAMP not null default CURRENT_TIMESTAMP,
       endtime TIMESTAMP not null,
       thumbnail int
);

create table if not exists event_managers (
       event_id int not null,
       user_id int not null,
       primary key(event_id, user_id)
);

create table if not exists event_entries (
       event_id int not null,
       participant_id int not null,
       registered TIMESTAMP not null,
       primary key (event_id, participant_id)
);

alter table users add foreign key (id) references event_participants(id) on delete cascade;
alter table teams add foreign key (id) references event_participants(id) on delete cascade;

alter table team_participants add foreign key (user_id) references users(id) on delete cascade;
alter table team_participants add foreign key (team_id) references teams(id) on delete cascade;

alter table events add foreign key (thumbnail) references images(id) on delete set null;

alter table event_entries add foreign key (event_id) references events(id) on delete cascade;
alter table event_entries add foreign key (participant_id) references event_participants(id) on delete cascade;

alter table event_managers add foreign key (event_id) references events(id) on delete cascade;
alter table event_managers add foreign key (user_id) references users(id) on delete cascade;
