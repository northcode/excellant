﻿#! python
import sys,os,hashlib,smtplib,string,random
from email.mime.text import MIMEText
from flask import Flask,render_template,request,url_for,flash,redirect,session,abort
from werkzeug import check_password_hash, generate_password_hash
import re
from flask.ext.login import LoginManager,login_user,logout_user,login_required,confirm_login,current_user
import markdown2
from splashes import get_random_splash,get_random_insult

#  ------------------------- APP CONFIG  --------------------------------

basedir = os.path.abspath(os.path.dirname(__file__))

from app import *
from eskedb import *

app.secret_key = 'ntmyisnice'

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)

mail_from = 'eskelan.com@jamieswhiteshirt.com'
mail_login = 'excellant'

signup_mail = """
<p>Velkommen til EskeLAN! Du har blitt tildelt en bruker på <a href="http://www.eskelan.com/login">eskelan.com</a> som du vil trenge for å melde deg på diverse events.</p>
<p>Ditt brukernavn: {}</p>
<p>Ditt passord: {}</p>
<p>Vennligst endre passordet så fort du logger inn.</p>
"""

resetpassword_mail = """
<p>Din bruker på <a href="http://www.eskelan.com/login">eskelan.com</a> har fått et nytt tilfeldig passord fordi du ba om et nytt passord.</p>
<p>Ditt brukernavn: {}</p>
<p>Ditt nye passord: {}</p>
<p>Vennligst endre passordet så fort du logger inn.</p>
"""

# --------------------------  UTIL FUNCTIONS -----------------------------

def restrict(assertion):
    if not assertion:
        abort(403)

def needs(assertion):
    if not assertion:
        abort(500)

def exists(assertion):
    if not assertion:
        abort(404)

def randompassword(length):
    chars = string.ascii_letters + string.digits
    return ''.join((random.choice(chars)) for x in range(length))

def adduser(email):
    password = randompassword(10)
    
    msg = MIMEText(signup_mail.format(email, password), 'html')
    msg['Subject'] = 'Innlogging på eskelan.com'
    msg['From'] = mail_from
    msg['To'] = email
    
    smtpObj = smtplib.SMTP('send.one.com')
    smtpObj.login(mail_from,mail_login)
    smtpObj.sendmail(mail_from, email, msg.as_string())
    smtpObj.quit()

    newuser = User()
    newuser.participantname = email
    newuser.itype = 'user'
    newuser.email = email
    newuser.password = generate_password_hash(password)
    newuser.userlevel = 1
    db.session.add(newuser)
    db.session.commit()

def resetpassword(userid):
    user = User.query.get(userid)
    if not user:
        return False
    
    password = randompassword(10)
    
    msg = MIMEText(resetpassword_mail.format(user.email, password), 'html')
    msg['Subject'] = 'Resatt passord på eskelan.com'
    msg['From'] = mail_from
    msg['To'] = user.email
    
    smtpObj = smtplib.SMTP('send.one.com')
    smtpObj.login(mail_from,mail_login)
    smtpObj.sendmail(mail_from, user.email, msg.as_string())
    smtpObj.quit()
    
    user.password = generate_password_hash(password)
    db.session.add(user)
    db.session.commit()
    return True

def deleteuser(userid):
    user = User.query.get(userid)
    db.session.delete(user)
    db.session.commit()

def addteam(name):
    newteam = Team()
    newteam.participantname = name
    db.session.add(newteam)
    db.session.commit()
    
    setteammember(newteam.id,current_user.id,True)

def deleteteam(teamid):
    team = Team.query.get(teamid)
    if team:
        db.session.delete(team)
        db.session.commit()
        return True
    else:
        return False

def setteammember(teamid,userid,ismod=False):
    newteamparticipant = TeamParticipant()
    user = User.query.get(userid)
    team = Team.query.get(teamid)
    if team and user:
        teamparticipant = TeamParticipant.query.filter_by(user_id=user.id,team_id=team.id).first()
        if not teamparticipant:
            teamparticipant = TeamParticipant()
            teamparticipant.user_id = user.id
            teamparticipant.team_id = team.id
            teamparticipant.ismod = 1 if ismod else 0
            db.session.add(teamparticipant)
            db.session.commit()
        return True
    else:
        return False

def deleteteammember(teamid,userid):
    user = User.query.get(userid)
    team = Team.query.get(teamid)
    if team and user:
        team.users.remove(user)
        db.session.commit()
        return True
    return False

def addevent(title):
    event = Event()
    event.title = title
    event.description = '*Dette er eventsiden for ' + title + '*'
    db.session.add(event)
    db.session.commit()
    
    return event

def make_login(username,password):
    usersmatching = User.query.filter_by(email=username).all()
    has_match = len(usersmatching) > 0
    if not has_match:
        return False
    user = usersmatching[0]
    if not check_password_hash(user.password,password):
        return False
    session['logged_in'] = True
    login_user(user, remember=True)
    return True

def make_logout():
    session.clear()
    logout_user()
    return not is_logged_in()

def edituser(uid,username,password,confpass):
    user = User.query.get(uid)
    if username != "":
        user.participantname = username
    if password != "":
        if check_password_hash(user.password,confpass):
            user.password = generate_password_hash(password)
        else:
            return False
    db.session.add(user)
    db.session.commit()
    return True

def setuserlevel(uid,level):
    level = int(level)
    if level < 0:
        return True
    user = User.query.get(uid)
    if user.id != current_user.id:
        user.userlevel = level
        db.session.add(user)
        db.session.commit()
        return True
    return False

def editevent(id,title,desc):
    event = Event.query.get(id)
    event.title = title
    event.description = desc
    db.session.add(event)
    db.session.commit()
    return True

def addeventadmin(id,username):
    event = Event.query.get(id)
    user = User.query.filter_by(participantname=username).first()
    if not event or not user:
        return False
    eventadmin = EventManager()
    eventadmin.event_id = event.id
    eventadmin.user_id = user.id
    db.session.add(eventadmin)
    db.session.commit()
    return True

def joinevent(id,uid):
    event = Event.query.get(id)
    participant = EventParticipant.query.get(uid)
    if not event or not participant:
        return False
    evententry = EventEntry()
    evententry.event_id = event.id
    evententry.participant_id = participant.id
    db.session.add(evententry)
    db.session.commit()
    return True

def leaveevent(id,uid):
    event = Event.query.get(id)
    participant = EventParticipant.query.get(uid)
    if not event or not participant:
        return False
    evententry = EventEntry.query.filter_by(event_id = event.id, participant_id = participant.id).first()
    db.session.delete(evententry)
    db.session.commit()
    return True

unameregex = re.compile('[^a-zA-Z0-9_]')
dnameregex = re.compile('[^a-zA-Z\s]')
emailregex = re.compile('[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}')

# ------------------------- JIJNA FUNCTIONS ------------------------------

def testregex(str, reg):
    return bool(reg.search(str))

def lorem():
    return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ultricies malesuada elit id dapibus. Fusce scelerisque felis neque, sit amet commodo velit sodales et. Donec et pellentesque turpis, a sollicitudin dolor. Quisque sem odio, tristique ut nulla at, euismod aliquam nibh. Vestibulum metus turpis, fermentum ac nunc non, dignissim efficitur elit. Nullam nulla sapien, fringilla eu gravida nec, ultricies at ligula. Nam ullamcorper diam odio, ac iaculis massa tristique eget. Praesent aliquam turpis vitae elit euismod, sed scelerisque tortor ornare. Nulla ut quam urna. Nulla venenatis aliquet tellus, pretium ultrices tortor. Etiam at libero gravida velit volutpat lacinia fermentum vel nunc. Phasellus eleifend dolor vitae est eleifend, id malesuada augue elementum. Donec at vehicula nisi. Duis felis ligula, molestie vitae mattis nec, cursus vitae nisi.";

app.jinja_env.globals.update(lorem=lorem)
app.jinja_env.globals.update(splash=get_random_splash)
app.jinja_env.globals.update(insult=get_random_insult)
app.jinja_env.globals.update(is_logged_in=is_logged_in)
app.jinja_env.filters['markdown'] = markdown2.markdown

# ------------------------- ERROR HANDLERS  ------------------------------

@app.errorhandler(404)
def render_404(e):
    return render_template('errors/404.html'), 404

@app.errorhandler(401)
def render_401(e):
    return render_template('errors/401.html'), 401

@app.errorhandler(403)
def render_403(e):
    return render_template('errors/403.html'), 403

@app.errorhandler(500)
def render_500(e):
    return render_template('errors/500.html'), 500

# ------------------------- APP ROUTES  ----------------------------------

@app.route('/debug/adduser/<email>')
def debug_adduser(email):
    if app.debug:
        adduser(email)
        return redirect('/')
    else:
        abort(500)

@app.route('/request/adduser', methods=['POST'])
@login_required
def request_adduser():
    needs('email' in request.form)
    restrict(current_user.is_mod())
    adduser(request.form['email'])
    return redirect('/admin')

@app.route('/request/addteam', methods=['POST'])
@login_required
def request_addteam():
    needs('name' in request.form)
    addteam(request.form['name'])
    team = Team.query.filter_by(participantname=request.form['name']).first()
    needs(team)
    return redirect(team.get_url())

@app.route('/request/deleteteam', methods=['POST'])
@login_required
def request_deleteteam():
    needs('teamid' in request.form)
    restrict(current_user.get_membership(request.form['teamid']).ismod)
    deleteteam(request.form['teamid'])
    return redirect('/')

@app.route('/request/setteammember', methods=['POST'])
def request_setteammember():
    needs('teamid' in request.form)
    needs('username' in request.form)
    user = User.query.filter_by(participantname=request.form['username']).first()
    needs(user)
    setteammember(request.form['teamid'],user.id,False)
    team = Team.query.get(request.form['teamid'])
    needs(team)
    return redirect(team.get_url())

@app.route('/request/deleteteammember', methods=['POST'])
def request_deleteteammember():
    needs('teamid' in request.form)
    needs('userid' in request.form)
    needs(deleteteammember(request.form['teamid'],request.form['userid']))
    team = Team.query.get(request.form['teamid'])
    needs(team)
    return redirect(team.get_url())

@app.route('/request/addevent', methods=['POST'])
def request_addevent():
    needs('title' in request.form)
    event = addevent(request.form['title'])
    needs(event)
    return redirect(event.get_url())

@app.route('/request/editevent', methods=['POST'])
def request_editevent():
    needs('eventid' in request.form)
    needs('title' in request.form)
    needs('description' in request.form)
    needs(editevent(request.form['eventid'],request.form['title'],request.form['description']))
    event = Event.query.get(request.form['eventid'])
    return redirect(event.get_url())

@app.route('/request/addeventadmin', methods=['POST'])
@login_required
def request_addeventadmin():
    needs('eventid' in request.form)
    needs('username' in request.form)
    needs(addeventadmin(request.form['eventid'],request.form['username']))
    event = Event.query.get(request.form['eventid'])
    return redirect(event.get_url())

@app.route('/request/joinevent', methods=['POST'])
@login_required
def request_joinevent():
    needs('eventid' in request.form)
    needs('participantid' in request.form)
    
    team = Team.query.get(request.form['participantid'])
    if team:
        membership = current_user.get_membership(team.id)
        restrict(membership)
        restrict(membership.ismod)
    
    needs(joinevent(request.form['eventid'], request.form['participantid']))
    event = Event.query.get(request.form['eventid'])
    return redirect(event.get_url())

@app.route('/request/leaveevent', methods=['POST'])
@login_required
def request_leaveevent():
    needs('eventid' in request.form)
    needs('participantid' in request.form)
    needs(leaveevent(request.form['eventid'], request.form['participantid']))
    event = Event.query.get(request.form['eventid'])
    return redirect(event.get_url())

@app.route('/request/deleteuser', methods=['POST'])
def request_deleteuser():
    needs('userid' in request.form)
    deleteuser(request.form['userid'])
    return redirect('/')

@app.route('/request/resetpassword', methods=['POST'])
def request_resetpassword():
    needs('userid' in request.form)
    if resetpassword(request.form['userid']):
        return redirect('/admin')

@app.route('/maketeam')
@login_required
def maketeam():
    return render_template('maketeam.html')

@app.route('/user/<participantname>')
def user(participantname):
    user = User.query.filter_by(participantname=participantname).first()
    if user:
        return render_template('user.html', user=user)
    else:
        abort(404)

@app.route('/team/<participantname>')
def team(participantname):
    team = Team.query.filter_by(participantname=participantname).first()
    exists(team)
    return render_template('team.html', team=team)

@app.route('/event/<int:id>/<title>')
def event(id,title):
    event = Event.query.get(id)
    exists(event)
    return render_template('event.html', event=event)

@app.route('/admin')
def adminpage():
    needs(current_user.is_mod())
    return render_template('admin.html', users=User.query.all(), teams=Team.query.all(), events=Event.query.all())

@app.route('/login', methods=['GET','POST'])
def loginpage():
    if 'username' in request.form:
        if make_login(request.form['username'],request.form['password']):
            return redirect('/')
        else:
            return render_template('login.html', error="Feil brukernavn eller passord.")
    else:
        return render_template('login.html')

@app.route('/resetpassword', methods=['GET','POST'])
def resetpasswordpage():
    if 'email' in request.form:
        user = User.query.filter_by(email=request.form['email']).first()
        if user:
            if resetpassword(user.id):
                return render_template('resetpassword.html',message='Et nytt passord har blitt sendt til ' + request.form['email'])
            else:
                abort(500)
        else:
            return render_template('resetpassword.html',error='Ingen brukere er registrert med epostaddresse ' + request.form['email'])
    return render_template('resetpassword.html')

@app.route('/logout')
@login_required
def logoutpage():
    needs(make_logout())
    return redirect('/')

@app.route('/edituser/<id>', methods=['GET','POST'])
@login_required
def edituserpage(id):
    id = int(id)
    user = User.query.get(id)
    exists(user)
    
    if not 'username' in request.form:
        return render_template('edituser.html', user=user)
    restrict(current_user.id == id or current_user.is_admin())
    if 'userlevel' in request.form:
        if not current_user.is_admin():
            abort(403)
        else:
            setuserlevel(id, request.form['userlevel'])
    if request.form['password'] != "":
        if not request.form['newpass'] == request.form['confpass']:
            return render_template("edituser.html", error="Nye passord matcher ikke", user=user)
    if edituser(id,request.form['username'],request.form['newpass'],request.form['password']):
        return render_template('edituser.html', message = "Brukerinfo endret", user=user)
    else:
        return render_template('edituser.html', error="Gammelt passord stemmer ikke", user=user)

@app.route('/')
def index():
    events= Event.query.all()
    return render_template('articles.html', events = events)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
