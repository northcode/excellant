from sqlalchemy.dialects.mysql import TINYINT
from app import is_logged_in
from database import db

class EventParticipant(db.Model):
    __tablename__ = "event_participants"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    participantname = db.Column(db.String(50), nullable=False, unique=True)
    itype = db.Column(db.String(30), nullable=False)
    __mapper_args__ = {
        'polymorphic_on':itype
    }

class User(EventParticipant):
    __tablename__ = "users"
    id = db.Column(db.Integer, db.ForeignKey(EventParticipant.id), primary_key=True, nullable=False)
    email = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(512), nullable=False)
    userlevel = db.Column(TINYINT, nullable=False)
    __mapper_args__ = {
        'polymorphic_identity':'user'
    }
    
    def get_membership(self,team_id):
        return TeamParticipant.query.filter_by(user_id=self.id,team_id=team_id).first()

    def is_active(self):
        return self.userlevel > 0
    
    def get_id(self):
        return self.id

    def is_authenticated(self):
        return is_logged_in()

    def is_anonymous(self):
        return False
    
    def get_url(self):
        return "/user/" + self.participantname

    def is_mod(self):
        return self.userlevel > 1
    
    def is_admin(self):
        return self.userlevel > 2

    def is_event_admin(self,eventid):
        return len(EventManager.query.filter_by(event_id = eventid, user_id= self.id).all()) > 0 or self.is_mod()

class Team(EventParticipant):
    __tablename__ = "teams"
    id = db.Column(db.Integer, db.ForeignKey(EventParticipant.id), primary_key=True, nullable=False)
    __mapper_args__ = {
        'polymorphic_identity':'team'
    }
    
    users = db.relationship(User, secondary='team_participants', backref='teams')
    
    def get_url(self):
        return "/team/" + self.participantname

class TeamParticipant(db.Model):
    __tablename__ = "team_participants"
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), primary_key=True, nullable=False)
    team_id = db.Column(db.Integer, db.ForeignKey(Team.id), primary_key=True, nullable=False)
    ismod = db.Column(TINYINT, default=0, nullable=False)

class Image(db.Model):
    __tablename__ = "images"
    id = db.Column(db.Integer, primary_key=True)
    filepath = db.Column(db.String(255), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    events = db.relationship('Event', backref = 'events')

class Event(db.Model):
    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(32767), nullable=False)
    starttime = db.Column(db.TIMESTAMP, nullable=False)
    endtime = db.Column(db.TIMESTAMP, nullable=False)
    thumbnail = db.Column(db.Integer, db.ForeignKey(Image.id))
    
    eventmanagers = db.relationship(User, secondary='event_managers', backref='managedevents')
    participants = db.relationship(EventParticipant, secondary='event_entries', backref='events')

    def is_participant(self, participantid):
        return len(EventEntry.query.filter_by(event_id=self.id, participant_id=participantid).all()) > 0
    
    def get_url(self):
        return '/event/' + str(self.id) + '/' + self.title

class EventManager(db.Model):
    __tablename__ = "event_managers"
    event_id = db.Column(db.Integer, db.ForeignKey(Event.id), primary_key=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), primary_key=True, nullable=False)

class EventEntry(db.Model):
    __tablename__ = "event_entries"
    event_id = db.Column(db.Integer, db.ForeignKey(Event.id), primary_key=True, nullable=False)
    participant_id = db.Column(db.Integer, db.ForeignKey(EventParticipant.id), primary_key=True, nullable=False)
    registered = db.Column(db.TIMESTAMP, nullable=False)
